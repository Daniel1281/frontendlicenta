import { createSwitchNavigator, createAppContainer } from "react-navigation";
import LaunchScreen from "../containers/LaunchScreen";
import DayComponent from "../components/DayComponent";
import LogIn from "../components/LogIn"

const AppNavigatior = createSwitchNavigator(

    {
      LaunchScreen:LaunchScreen,
      DayComponent:DayComponent,
      LogInScreen: LogIn,
    },
    {
      initialRouteName:'DayComponent'
    }
)
export default createAppContainer(AppNavigatior)
