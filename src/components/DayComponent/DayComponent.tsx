import React, {Component} from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import EntryComponent from "../EntryComponent";

const DayComponent = () => {

    return  (
        <View style={styles.mainView}>
            <View style={styles.top}>
                <Text style={styles.topText}>Monday - 21.01.2019</Text>
            </View>
            <View>
                <EntryComponent startHour="10:00" endHour="12:00" name="Analiza" room="2/I" type="lab"/>
                <EntryComponent startHour="12:00" endHour="14:00" name="Analiza1" room="2/I" type="lab"/>
                <EntryComponent startHour="14:00" endHour="16:00" name="Analiza2" room="2/I" type="lab"/>
                <EntryComponent startHour="16:00" endHour="18:00" name="Analiza3" room="2/I" type="lab"/>
            </View>
        </View>
    );

};

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: '#ededed'
    },
    top: {
        padding: 20,
        backgroundColor: '#197466'
    },
    topText: {
        fontSize: 24,
        textAlign: 'center',
        color: 'white'
    },
});

export default DayComponent;

