import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

const LogIn = () => {

    return <View style={styles.elements}>
        <View style={styles.inputField}>
            <TextInput style={styles.textInput} placeholder = "SCS Email" />
        </View>
        <View style={styles.inputField}>
            <TextInput style={styles.textInput} placeholder = "SCS Password"/>
        </View>
        <View style={styles.button}>
            <Text style={styles.textButton}>Log In</Text>
        </View>
    </View>
};

const styles = StyleSheet.create({
    elements: {
        alignItems: 'center',
        paddingTop: 200,
        flex: 1,
        backgroundColor: '#ededed'
    },
    inputField: {
        paddingBottom: 20,
    },
    textInput: {
        height: 50, 
        width: 370, 
        borderColor: 'gray', 
        borderWidth: 1,
        paddingLeft: 7,
        backgroundColor: 'white'
    },
    button: {
        backgroundColor: '#197466',
        height: 50, 
        width: 370,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textButton: {
        color: 'white',
        fontSize: 20
    }
});

export default LogIn; 