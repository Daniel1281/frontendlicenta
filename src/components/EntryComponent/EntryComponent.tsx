import React from 'react';
import { View, Text, StyleSheet} from 'react-native';

const EntryComponent = props => {

    return <View style={styles.entry}>
        <View style={styles.rightSide}>
            <Text style={styles.hourStyle}>  {props.startHour}  </Text>
            <Text style={styles.nameText}>  {props.name}  </Text>
        </View>
        <View style={styles.leftSide}>
            <Text style={styles.hourStyle}>  {props.endHour}  </Text>
            <Text style={styles.text}>  {props.type}  </Text>
            <Text style={styles.text}>  {props.room}  </Text>
        </View>
    </View>
};

const styles = StyleSheet.create({
    entry: {
        padding: 10,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 12,},
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    rightSide: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
    },
    text: {
        fontSize: 20,
        color: '#636363',
        fontWeight: '300'
    },
    nameText: {
        fontSize: 24,
        color: '#197466',
        fontWeight: '100'
    },
    leftSide: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
    },
    hourStyle: {
        fontSize: 20,
        color: '#636363',
        fontWeight: '300'
    }
});

export default EntryComponent; 